#!/usr/bin/env python3

import pprint, signal, sys, time
from collections import Counter
from envparse import env
from rpi_rf import RFDevice

env.read_envfile()

# RF settings
RF_GPIO_RX = env.int('RF_GPIO_RX', default=27)
RF_READCODES_TIMEOUT = env.int('RF_READCODES_TIMEOUT', default=2)
RF_READCODES_UNITS = env.list('RF_READCODES_UNITS', default=['A', 'B', 'C', 'D'])
RF_READCODES_STATES = env.list('RF_READCODES_STATES', default=['ON', 'OFF'])

rfdevice = None

# pylint: disable=unused-argument
def exithandler(signal, frame):
    rfdevice.cleanup()
    sys.exit(0)

signal.signal(signal.SIGINT, exithandler)

print(f"Attaching to GPIO {str(RF_GPIO_RX)}")
rfdevice = RFDevice(RF_GPIO_RX)
rfdevice.enable_rx()

timestamp = None

system_code = input('For which system code do you want to read the RF codes? ')
codes_read_tx = { system_code: {} }
codes_read_rx = {}
for unit in RF_READCODES_UNITS:
    codes_read_tx[system_code][unit] = {}
    for state in RF_READCODES_STATES:
        cnt = Counter()
        time_end = time.time() + RF_READCODES_TIMEOUT

        print(f"Listening for the next {RF_READCODES_TIMEOUT} seconds ...")
        print(f"Please send RF code for unit {unit}: {state}")

        while time.time() < time_end:
            if rfdevice.rx_code_timestamp != timestamp:
                timestamp = rfdevice.rx_code_timestamp
                cnt[rfdevice.rx_code] += 1
            time.sleep(0.2)
  
        # Some remotes send different RF codes. The code with the highest occurrence is probably the one that we want
        if len(cnt) > 0:
            (rf_code, rf_code_count) = cnt.most_common(1)[0]
            codes_read_tx[system_code][unit][state] = rf_code
            codes_read_rx[rf_code] = { "system_code": system_code, "unit": unit, "state": state }

            print(f"RF Code {rf_code} has been received for unit {unit}: {state}\n")
        else:
            print(f"No RF code received for unit {unit}: {state}\n")

        # Sleep a second to not pollute the capturing for the next RF code
        time.sleep(1)

rfdevice.cleanup()

# Disable PrettyPrinter's dictionary sorting
# https://stackoverflow.com/questions/25683088/disabling-sorting-mechanism-in-pprint-output
python_version = int(f"{sys.version_info.major}{sys.version_info.minor}{sys.version_info.micro}")
if python_version > 380:
    pp = pprint.PrettyPrinter(indent=4, sort_dicts=False)
else:
    if python_version > 370:
        pprint.sorted = lambda x, key=None: x
    else:
        pprint._sorted = lambda x:x
    pp = pprint.PrettyPrinter(indent=4)

print("That's it. Use the following dictionaries and add them to the rf_remote_codes module.")
print("RX = ")
pp.pprint(codes_read_rx)
print("TX = ")
pp.pprint(codes_read_tx)
