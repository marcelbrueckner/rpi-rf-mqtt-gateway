# rpi-rf-mqtt-gateway

A Python script that listens for signals of a basic RF remote on a GPIO port and publishs that information to MQTT. This script is based on half of each [milaq/rpi-rf/scripts/rpi-rf_receive](https://github.com/milaq/rpi-rf/blob/master/scripts/rpi-rf_receive) and [robmarkcole/rpi-rf-mqtt](https://github.com/robmarkcole/rpi-rf-mqtt).

## Configuration

### MQTT topic(s)

Per default, the remote's signals will be published to separate topics per system code (`{{ RF_SYSTEM_CODE }}`) and unit (`{{ RF_UNIT }}`). Assuming you're having set your socket's system code to `00000`, the topics would be composed as follows.

```bash
# If RF code could be identified
# MQTT_TOPIC=rf/{{ RF_SYSTEM_CODE }}/{{ RF_UNIT }}
# The message that will be sent has a payload of either ON or OFF
rf/00000/A
rf/00000/B
rf/00000/C
rf/00000/D

# If RF code could NOT be identified
# MQTT_TOPIC_UNKNOWN=rf/{{ RF_CODE }}
# The message that will be sent has always a payload of ON
rf/5588305
rf/5588308
rf/5591377
rf/5591380
rf/5592145
rf/5592148
rf/5592337
rf/5592340
```

You can ignore unknown RF signals by setting `RF_IGNORE_UNKNOWN=TRUE`. This is especially useful if you're getting noise because of using a low-quality RF receiver module (e.g. a XY-MK-5V - I recommend RXB-6 or RXB-12 receivers).

The topic string is rendered using the Jinja2 templating engine. Thus you can easily use any filter available in Jinja2 to compose the final topic. Assuming you've assigned the above mentioned system code `00000` to a specific room in your home (like I did :wink:), for example `livingroom`, you want to give your topic a more descriptive name.

```bash
# MQTT_TOPIC=rf/{{ 'livingroom' if RF_SYSTEM_CODE == '00000' else RF_SYSTEM_CODE }}/{{ RF_UNIT }}
rf/livingroom/A
rf/livingroom/B
rf/livingroom/C
rf/livingroom/D
```

If your receiver needs to handle multiple system codes, you can provide an extra dictionary `MQTT_EXTRA_ENV=key1=value1,key2=value2,...` and use it in the `MQTT_TOPIC` template string as variable.

```bash
# MQTT_EXTRA_ENV=00000=livingroom,11011=bedroom,11111=kitchen
# MQTT_TOPIC=rf/{{ MQTT_EXTRA_ENV[RF_SYSTEM_CODE] | default(RF_SYSTEM_CODE) }}/{{ RF_UNIT }}
# - for system code 00000
rf/livingroom/A
rf/livingroom/B
rf/livingroom/C
rf/livingroom/D
# - for system code 11011
rf/bedroom/A
rf/bedroom/B
rf/bedroom/C
rf/bedroom/D
# - for system code 11000
rf/11000/A
rf/11000/B
rf/11000/C
rf/11000/D
```

## Usage

Undoubtedly, the main purpose of this script is to publish your RF remote's data (the event of pressing a button on your remote) to a MQTT broker. From this point you can do whatever you want with that data. Maybe you:

* have lost your RF controlled power sockets but happen to have a MQTT-enabled power socket (e.g. a Sonoff S20 Smart Socket flashed with Tasmota) that you want to control with your RF remote
* still have your RF controlled power sockets but you want to display the current state in your home automation dashboard and thus listening for your remote's events
* want to control multiple Philips Hue smart lights with a single remote instead of a pile of Hue dimmer switches

The latter is my use-case and I've implemented it with [Home Assistant](home-assistant.io) in a few easy steps. Assuming you're already having a working Home Assistant instance:

1. Set up this script as a systemd service

    * You can use [`templates/rfmqttgateway.service`](templates/rfmqttgateway.service) as template, adapt it to your needs and place it at `/etc/systemd/system/rfmqttgateway.service`
    * Run `sudo systemctl daemon-reload`
    * Run `sudo systemctl enable rfmqttgateway` to start the service after reboot
    * Run `sudo service rfmqttgateway start`
    * Verify the service via `sudo service rfmqttgateway status`

2. Set up an [automation](https://www.home-assistant.io/docs/automation/) in Home Assistant to pass the remote's command to the desired entities. Below automation uses Home Assistant's powerful [Automation Templating](https://www.home-assistant.io/docs/automation/templating/) feature for the service call action. This greatly reduces boilerplate and allows for a single instead of 8 (4 units * 2 states) automations.

    ```yaml
    - alias: livingroom_rf_remote
      trigger:
        - platform: mqtt
          topic: rf/livingroom/+
      action:
        - service_template: >
            light.turn_{{ trigger.payload | lower }}
          data_template:
            entity_id: >
              {% if trigger.topic.split('/')[-1] == 'A' %}
                light.entity_a
              {% elif trigger.topic.split('/')[-1] == 'B' %}
                light.entity_b_1,light.entity_b_2
              {% elif trigger.topic.split('/')[-1] == 'C' %}
                light.entity_c
              {% else %}
                light.entity_d
              {% endif %}
    ```

## Determine new RF remote codes

### Capture RF signals

A script `rpi-rf-readcodes.py` is provided which will ask to press your remote's buttons. I only have one sort of remotes (namely an [Elro AB440R remote](https://www.amazon.de/Elro-AB440R-Funkfernbedienung-4-Kanal-AB440-Serie/dp/B005IF87Q6) for the [AB440 series switches](https://www.amazon.de/Elro-AB440S-3C-Funksteckdosen-Funksteckdose/dp/B002QXN7X6)). Naturally, the script is heavily opinionated. Nevertheless, there are a few configuration options that may help you set up your brand of radio remote.

```ini
# When learning RF codes, specify ...
# ... how long (in seconds) should be listened for each command. Defaults to 2
RF_READCODES_TIMEOUT=2

# ... which buttons resp. units your remote has available. Defaults to A,B,C,D
RF_READCODES_UNITS=A,B,C,D

# ... which states can be set for every unit. Defaults to ON,OFF
RF_READCODES_STATES=ON,OFF
```

The script will output a summary like below when finished which can be used to add codes to the `rf_remote_codes` module.

```python
# For which system code do you want to read the RF codes? 11011
# Listening for the next 2 seconds ...
# Please send RF code for unit A: ON
# Code has been captured.

# ...

# That's it. Use the following dictionaries and add them to the rf_remote_codes module.
RX =
{   263505: {"RF_SYSTEM_CODE": '11011', "RF_UNIT": 'A', "RF_STATE": 'ON'},
    263508: {"RF_SYSTEM_CODE": '11011', "RF_UNIT": 'A', "RF_STATE": 'OFF'},
    266577: {"RF_SYSTEM_CODE": '11011', "RF_UNIT": 'B', "RF_STATE": 'ON'},
    266580: {"RF_SYSTEM_CODE": '11011', "RF_UNIT": 'B', "RF_STATE": 'OFF'},
    267345: {"RF_SYSTEM_CODE": '11011', "RF_UNIT": 'C', "RF_STATE": 'ON'},
    267348: {"RF_SYSTEM_CODE": '11011', "RF_UNIT": 'C', "RF_STATE": 'OFF'},
    267537: {"RF_SYSTEM_CODE": '11011', "RF_UNIT": 'D', "RF_STATE": 'ON'},
    267540: {"RF_SYSTEM_CODE": '11011', "RF_UNIT": 'D', "RF_STATE": 'OFF'}}
TX =
{   '11011': {   'A': {'ON': 263505, 'OFF': 263508},
                 'B': {'ON': 266577, 'OFF': 266580},
                 'C': {'ON': 267345, 'OFF': 267348},
                 'D': {'ON': 267537, 'OFF': 267540}}}
```

### Calculate

You may also be able to calculate the codes of your remote once you figured out a sample set of commands. For example, for my Elro remotes:

* Within an system code, the difference between the `ON` resp. `OFF` commands is always
  * `4,032` between `A` and `D`, split into
  * `3,072` between `A` and `B`,
  * `768` between `B` and `C` and
  * `192` between `C` and `D`.
* The `OFF` command is always the `ON` command's code plus `3`.
* Depending on which position the system code differs from the previous, the difference between the same unit and state of the different system codes is
  * `16,384` if the rightmost digit is different (e.g. `11111` and `11110`),
  * `65,536` if the penultimate digit is different (e.g. `11111` and `11101`),
  * `262,144` if the third to last digit is different (e.g. `11111` and `11011`)
  * `1,048,576` if the second digit is different (e.g. `11111` and `10111`),
  * `4,194,304` if the first digit is different (e.g. `11111` and `01111`).

As with everything computer-related, those codes are obviously binary-based. Looking at the differences above the

* five digits of the system code seem to correspond to binary digits `2^22`, `2^20`, `2^18`, `2^16` and `2^14`
* units correspond to binary digits `2^11`, `2^10`, `2^9`, `2^8`, `2^7` and `2^6`
* state corresponds to binary digits `2^1` and `2^0`
* remaining binary digits are always zero

If we group the codes together we get something like this binary representation (where the above mentioned digits are set to `1` and unused digits are `0`).

```plain
    1  0  1  0  1  0  1  0  1  0  0  1  1  1  1  1  1  0  0  0  0  1  1
2^x 22 21 20 19 18 17 16 15 14 13 12 11 10 09 08 07 06 05 04 03 02 01 00
   |__________________________|     |_________________|           |_____|
            system code                    UNITS                   STATE
```

As the system code increases, the resulting RF code should decrease. A unit further down the alphabet and the `OFF` command increases the resultung RF code. So we either have to invert all digits and add them to the base (which is the lowest RF code 1361) or substract the result of our calculation from the highest RF code we've captured (which is 5592340).

```bash
# System code 00000, unit A, state ON
# Captured RF code: 5588305
# - Invert input and add to 1361
Input:       0     0     0     0     0        1  1  1  1  1  1              1  1
Inverted:    1  0  1  0  1  0  1  0  1  0  0  0  0  0  0  0  0  0  0  0  0  0  0
        2^x 22 21 20 19 18 17 16 15 14 13 12 11 10 09 08 07 06 05 04 03 02 01 00
           |__________________________|     |_________________|           |_____|
                    SYSTEM CODE                    UNITS                   STATE

Result: 2^22 + 2^20 + 2^18 + 2^16 + 2^14 + 1,361 = 5,588,305

# - Subtract input from 5592340
Input:       0     0     0     0     0        1  1  1  1  1  1              1  1
        2^x 22 21 20 19 18 17 16 15 14 13 12 11 10 09 08 07 06 05 04 03 02 01 00
           |__________________________|     |_________________|           |_____|
                    SYSTEM CODE                    UNITS                   STATE

Result: 5,592,340 - (2^11 + 2^10 + 2^9 + 2^8 + 2^7 + 2^6 + 2^1 + 2^0) = 5,588,305
```

The script `rpi-rf-gencodes.py` does exactly that.

## To do

* For now, the script only listens to RF signals and publishes to a MQTT broker. I also want to implement it in the other direction: listening to a MQTT topic and sending the corresponding RF signals to turn on/off the radio controlled power sockets.
* Use a microcontroller (ESP32/ESP8266) instead of a Raspberry Pi as it seems to be overkill for this use case (or add more use-cases to the Raspberry Pi :smile:)
* Maybe drop this project and use [OpenMQTTGateway](https://github.com/1technophile/OpenMQTTGateway)
* Evaluate how the script can be adapted to support [Homie - An MQTT Convention for IoT/M2M](https://homieiot.github.io/)
* Home Assistant [MQTT Discovery](https://www.home-assistant.io/docs/mqtt/discovery/) (might conflict with Homie?)
* Send to multiple MQTT brokers
* Implement long button press
