#!/usr/bin/env python3

# Based on
# https://github.com/milaq/rpi-rf/blob/master/scripts/rpi-rf_receive
# https://github.com/robmarkcole/rpi-rf-mqtt/blob/master/rpi-rf-mqtt.py

import logging, signal, os, sys, time

from environs import Env
from marshmallow.validate import OneOf
from jinja2 import Template

from rpi_rf import RFDevice
from rf_remote_codes.elro import RX

from paho.mqtt.client import Client as MQTTClient

env = Env()
env.read_env()

# RF settings
RF_GPIO_RX = env.int('RF_GPIO_RX', default=27)
RF_GPIO_TX = env.int('RF_GPIO_TX', default=17)
RF_IGNORE_UNKNOWN = env.bool('RF_IGNORE_UNKNOWN', default=False)

# MQTT configuration
MQTT_BROKER_URL = env('MQTT_BROKER_URL')
MQTT_BROKER_USER = env('MQTT_BROKER_USER', default='')
MQTT_BROKER_PASS = env('MQTT_BROKER_PASS', default='')
MQTT_BROKER_PROTO = env(
    'MQTT_BROKER_PROTO',
    default='mqtt',
    validate=OneOf(
        ["mqtt", "ws"], error="MQTT_BROKER_PROTO must be one of: {choices}"
    )
)
if MQTT_BROKER_PROTO == 'mqtt':
    mqttBrokerDefaultPort = 1883
    mqttClientTransport = "tcp"
else:
    mqttBrokerDefaultPort = 9001
    mqttClientTransport = "websockets"
MQTT_BROKER_PORT = env('MQTT_BROKER_PORT', default=mqttBrokerDefaultPort)
MQTT_CLIENT_ID = env('MQTT_CLIENT_ID', default='rpi-rf-mqtt-gw')

MQTT_EXTRA_ENV=env.dict('MQTT_EXTRA_ENV', default=dict())
MQTT_TOPIC=env('MQTT_TOPIC', default='rf/{{ RF_SYSTEM_CODE }}/{{ RF_UNIT }}')
MQTT_TOPIC_UNKNOWN=env('MQTT_TOPIC_UNKNOWN', default='rf/{{ RF_RAW_CODE }}')

# LOG_FILE = env('LOG_FILE', default=f'{os.path.splitext(os.path.basename(__file__))[0]}.log')

# Initialize other configuration
logging.basicConfig(
    # filename=LOG_FILE, filemode='w',
    level=logging.INFO,
    datefmt='%Y-%m-%d %H:%M:%S', format='%(asctime)-15s - [%(levelname)s] %(module)s: %(message)s'
)

# Setup RF
rfdevice = RFDevice(RF_GPIO_RX)
rfdevice.enable_rx()

# pylint: disable=unused-argument
def exithandler(signal, frame):
    rfdevice.cleanup()
    sys.exit(0)
signal.signal(signal.SIGINT, exithandler)

def on_connect(client, userdata, flags, rc):
    logging.debug(f"Connected with result: {connack_string(rc)}")

def on_disconnect(client, userdata, rc):
    if rc != 0:
        logging.warn("Connection to MQTT broker has been interrupted unexpectingly.")

def on_publish(client, userdata, mid):
    logging.debug("Publish message to broker: SUCCESS")


def main():
    # Setup MQTT
    client = MQTTClient(client_id=MQTT_CLIENT_ID, transport=mqttClientTransport)
    client.on_connect = on_connect
    client.on_disconnect = on_disconnect
    client.on_publish = on_publish

    if MQTT_BROKER_USER != '' and MQTT_BROKER_PASS != '':
        client.username_pw_set(username=MQTT_BROKER_USER, password=MQTT_BROKER_PASS)
    logging.info(f"Connecting to {MQTT_BROKER_URL}")
    client.connect(MQTT_BROKER_URL)
    client.loop_start()

    logging.info("Listening for codes on GPIO " + str(RF_GPIO_RX))

    timestamp = None
    while True:
        if rfdevice.rx_code_timestamp != timestamp:
            timestamp = rfdevice.rx_code_timestamp

            raw_info = f"{str(rfdevice.rx_code)} [pulselength {str(rfdevice.rx_pulselength)}, protocol {str(rfdevice.rx_proto)}]"
            logging.debug(f"Received signal {raw_info}")

            local_env = os.environ.copy()
            local_env.update(env.dump())

            if rfdevice.rx_code in RX:
                local_env.update({ "RF_RAW_CODE": rfdevice.rx_code })
                local_env.update(RX[rfdevice.rx_code])
                system_code = RX[rfdevice.rx_code]["RF_SYSTEM_CODE"]
                unit = RX[rfdevice.rx_code]["RF_UNIT"]
                state = RX[rfdevice.rx_code]["RF_STATE"]
                
                template = Template(MQTT_TOPIC)
                mqttTopic = template.render(local_env)

                logging.debug(f"Received {unit}: {state} for system code {system_code}")
                logging.debug(f"Publish message {state} to topic {mqttTopic}")

                client.publish(mqttTopic, state, qos=1, retain=True)

            elif not RF_IGNORE_UNKNOWN:
                local_env.update({ "RF_RAW_CODE": rfdevice.rx_code })

                template = Template(MQTT_TOPIC_UNKNOWN)
                mqttTopic = template.render(local_env)

                logging.debug(f"Received unknown code {rfdevice.rx_code}")
                logging.debug(f"Publish message ON to topic {mqttTopic}")
                
                client.publish(mqttTopic, 'ON', qos=1, retain=True)

        time.sleep(0.2)

    rfdevice.cleanup()


if __name__ == "__main__":
    main()