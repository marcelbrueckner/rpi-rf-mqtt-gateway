#!/usr/bin/env python3

# Generate ELRO RF remote codes mapping for use with https://github.com/milaq/rpi-rf

# Subtract input (system code, unit and state) from 5592340
# Input:       1     1     1     1     1        1  1  1  1  1  1              1  1
#         2^x 22 21 20 19 18 17 16 15 14 13 12 11 10 09 08 07 06 05 04 03 02 01 00
#            |__________________________|     |_________________|           |_____|
#                     SYSTEM CODE                    UNITS                   STATE

from jinja2 import Environment, FileSystemLoader, Template

RF_CODE_BASE    = 5592340

UNIT          = {}
UNIT['A']     = '111111'
UNIT['B']     = '001111'
UNIT['C']     = '000011'
UNIT['D']     = '000000'

STATE         = {}
STATE['ON']   = '11'
STATE['OFF']  = '00'

RX = {}
TX = {}

# Every system code from 00000 (decimal 0) to 11111 (decimal 31)
for system_code_dec in range(0, 32):
    system_code = format(system_code_dec, '05b')
    RX[system_code] = {}
    TX[system_code] = {}
    
    rf_system_code = ''
    for index, digit in enumerate(system_code):
        rf_system_code = f'{rf_system_code}{digit}'
        if index < len(system_code) - 1:
            rf_system_code = f'{rf_system_code}0'

    for unit, unit_code in UNIT.items():
        TX[system_code][unit] = {}

        for state, state_code in STATE.items():
            rf_code = f'{rf_system_code}00{unit_code}0000{state_code}'
            rf_code_dec = str(RF_CODE_BASE - int(rf_code, base=2))

            TX[system_code][unit][state] = rf_code_dec
            RX[system_code][rf_code_dec] = { "RF_SYSTEM_CODE": system_code, "RF_UNIT": unit, "RF_STATE": state }

j2env = Environment(loader=FileSystemLoader('./templates/'), autoescape=True, trim_blocks=True, lstrip_blocks=True)
template = j2env.get_template("rf_remote_codes.py.j2")

with open('elro.py', 'w') as f:
    f.write(
        template.render({
            "RX": RX,
            "TX": TX 
        })
    )

print('RF codes written to elro.py')
