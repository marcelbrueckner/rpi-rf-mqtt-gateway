"""Provides a mapping from the ELRO 433 Mhz RF remote codes to the actual buttons and vice-versa."""

# RX - from received RF code to system code and unit state
RX = {

    # System code 00000
    5588305: { "RF_SYSTEM_CODE": "00000", "RF_UNIT": "A", "RF_STATE": "ON" },
    5588308: { "RF_SYSTEM_CODE": "00000", "RF_UNIT": "A", "RF_STATE": "OFF" },
    5591377: { "RF_SYSTEM_CODE": "00000", "RF_UNIT": "B", "RF_STATE": "ON" },
    5591380: { "RF_SYSTEM_CODE": "00000", "RF_UNIT": "B", "RF_STATE": "OFF" },
    5592145: { "RF_SYSTEM_CODE": "00000", "RF_UNIT": "C", "RF_STATE": "ON" },
    5592148: { "RF_SYSTEM_CODE": "00000", "RF_UNIT": "C", "RF_STATE": "OFF" },
    5592337: { "RF_SYSTEM_CODE": "00000", "RF_UNIT": "D", "RF_STATE": "ON" },
    5592340: { "RF_SYSTEM_CODE": "00000", "RF_UNIT": "D", "RF_STATE": "OFF" },

    # System code 00001
    5571921: { "RF_SYSTEM_CODE": "00001", "RF_UNIT": "A", "RF_STATE": "ON" },
    5571924: { "RF_SYSTEM_CODE": "00001", "RF_UNIT": "A", "RF_STATE": "OFF" },
    5574993: { "RF_SYSTEM_CODE": "00001", "RF_UNIT": "B", "RF_STATE": "ON" },
    5574996: { "RF_SYSTEM_CODE": "00001", "RF_UNIT": "B", "RF_STATE": "OFF" },
    5575761: { "RF_SYSTEM_CODE": "00001", "RF_UNIT": "C", "RF_STATE": "ON" },
    5575764: { "RF_SYSTEM_CODE": "00001", "RF_UNIT": "C", "RF_STATE": "OFF" },
    5575953: { "RF_SYSTEM_CODE": "00001", "RF_UNIT": "D", "RF_STATE": "ON" },
    5575956: { "RF_SYSTEM_CODE": "00001", "RF_UNIT": "D", "RF_STATE": "OFF" },

    # System code 00010
    5522769: { "RF_SYSTEM_CODE": "00010", "RF_UNIT": "A", "RF_STATE": "ON" },
    5522772: { "RF_SYSTEM_CODE": "00010", "RF_UNIT": "A", "RF_STATE": "OFF" },
    5525841: { "RF_SYSTEM_CODE": "00010", "RF_UNIT": "B", "RF_STATE": "ON" },
    5525844: { "RF_SYSTEM_CODE": "00010", "RF_UNIT": "B", "RF_STATE": "OFF" },
    5526609: { "RF_SYSTEM_CODE": "00010", "RF_UNIT": "C", "RF_STATE": "ON" },
    5526612: { "RF_SYSTEM_CODE": "00010", "RF_UNIT": "C", "RF_STATE": "OFF" },
    5526801: { "RF_SYSTEM_CODE": "00010", "RF_UNIT": "D", "RF_STATE": "ON" },
    5526804: { "RF_SYSTEM_CODE": "00010", "RF_UNIT": "D", "RF_STATE": "OFF" },

    # System code 00011
    5506385: { "RF_SYSTEM_CODE": "00011", "RF_UNIT": "A", "RF_STATE": "ON" },
    5506388: { "RF_SYSTEM_CODE": "00011", "RF_UNIT": "A", "RF_STATE": "OFF" },
    5509457: { "RF_SYSTEM_CODE": "00011", "RF_UNIT": "B", "RF_STATE": "ON" },
    5509460: { "RF_SYSTEM_CODE": "00011", "RF_UNIT": "B", "RF_STATE": "OFF" },
    5510225: { "RF_SYSTEM_CODE": "00011", "RF_UNIT": "C", "RF_STATE": "ON" },
    5510228: { "RF_SYSTEM_CODE": "00011", "RF_UNIT": "C", "RF_STATE": "OFF" },
    5510417: { "RF_SYSTEM_CODE": "00011", "RF_UNIT": "D", "RF_STATE": "ON" },
    5510420: { "RF_SYSTEM_CODE": "00011", "RF_UNIT": "D", "RF_STATE": "OFF" },

    # System code 00100
    5326161: { "RF_SYSTEM_CODE": "00100", "RF_UNIT": "A", "RF_STATE": "ON" },
    5326164: { "RF_SYSTEM_CODE": "00100", "RF_UNIT": "A", "RF_STATE": "OFF" },
    5329233: { "RF_SYSTEM_CODE": "00100", "RF_UNIT": "B", "RF_STATE": "ON" },
    5329236: { "RF_SYSTEM_CODE": "00100", "RF_UNIT": "B", "RF_STATE": "OFF" },
    5330001: { "RF_SYSTEM_CODE": "00100", "RF_UNIT": "C", "RF_STATE": "ON" },
    5330004: { "RF_SYSTEM_CODE": "00100", "RF_UNIT": "C", "RF_STATE": "OFF" },
    5330193: { "RF_SYSTEM_CODE": "00100", "RF_UNIT": "D", "RF_STATE": "ON" },
    5330196: { "RF_SYSTEM_CODE": "00100", "RF_UNIT": "D", "RF_STATE": "OFF" },

    # System code 00101
    5309777: { "RF_SYSTEM_CODE": "00101", "RF_UNIT": "A", "RF_STATE": "ON" },
    5309780: { "RF_SYSTEM_CODE": "00101", "RF_UNIT": "A", "RF_STATE": "OFF" },
    5312849: { "RF_SYSTEM_CODE": "00101", "RF_UNIT": "B", "RF_STATE": "ON" },
    5312852: { "RF_SYSTEM_CODE": "00101", "RF_UNIT": "B", "RF_STATE": "OFF" },
    5313617: { "RF_SYSTEM_CODE": "00101", "RF_UNIT": "C", "RF_STATE": "ON" },
    5313620: { "RF_SYSTEM_CODE": "00101", "RF_UNIT": "C", "RF_STATE": "OFF" },
    5313809: { "RF_SYSTEM_CODE": "00101", "RF_UNIT": "D", "RF_STATE": "ON" },
    5313812: { "RF_SYSTEM_CODE": "00101", "RF_UNIT": "D", "RF_STATE": "OFF" },

    # System code 00110
    5260625: { "RF_SYSTEM_CODE": "00110", "RF_UNIT": "A", "RF_STATE": "ON" },
    5260628: { "RF_SYSTEM_CODE": "00110", "RF_UNIT": "A", "RF_STATE": "OFF" },
    5263697: { "RF_SYSTEM_CODE": "00110", "RF_UNIT": "B", "RF_STATE": "ON" },
    5263700: { "RF_SYSTEM_CODE": "00110", "RF_UNIT": "B", "RF_STATE": "OFF" },
    5264465: { "RF_SYSTEM_CODE": "00110", "RF_UNIT": "C", "RF_STATE": "ON" },
    5264468: { "RF_SYSTEM_CODE": "00110", "RF_UNIT": "C", "RF_STATE": "OFF" },
    5264657: { "RF_SYSTEM_CODE": "00110", "RF_UNIT": "D", "RF_STATE": "ON" },
    5264660: { "RF_SYSTEM_CODE": "00110", "RF_UNIT": "D", "RF_STATE": "OFF" },

    # System code 00111
    5244241: { "RF_SYSTEM_CODE": "00111", "RF_UNIT": "A", "RF_STATE": "ON" },
    5244244: { "RF_SYSTEM_CODE": "00111", "RF_UNIT": "A", "RF_STATE": "OFF" },
    5247313: { "RF_SYSTEM_CODE": "00111", "RF_UNIT": "B", "RF_STATE": "ON" },
    5247316: { "RF_SYSTEM_CODE": "00111", "RF_UNIT": "B", "RF_STATE": "OFF" },
    5248081: { "RF_SYSTEM_CODE": "00111", "RF_UNIT": "C", "RF_STATE": "ON" },
    5248084: { "RF_SYSTEM_CODE": "00111", "RF_UNIT": "C", "RF_STATE": "OFF" },
    5248273: { "RF_SYSTEM_CODE": "00111", "RF_UNIT": "D", "RF_STATE": "ON" },
    5248276: { "RF_SYSTEM_CODE": "00111", "RF_UNIT": "D", "RF_STATE": "OFF" },

    # System code 01000
    4539729: { "RF_SYSTEM_CODE": "01000", "RF_UNIT": "A", "RF_STATE": "ON" },
    4539732: { "RF_SYSTEM_CODE": "01000", "RF_UNIT": "A", "RF_STATE": "OFF" },
    4542801: { "RF_SYSTEM_CODE": "01000", "RF_UNIT": "B", "RF_STATE": "ON" },
    4542804: { "RF_SYSTEM_CODE": "01000", "RF_UNIT": "B", "RF_STATE": "OFF" },
    4543569: { "RF_SYSTEM_CODE": "01000", "RF_UNIT": "C", "RF_STATE": "ON" },
    4543572: { "RF_SYSTEM_CODE": "01000", "RF_UNIT": "C", "RF_STATE": "OFF" },
    4543761: { "RF_SYSTEM_CODE": "01000", "RF_UNIT": "D", "RF_STATE": "ON" },
    4543764: { "RF_SYSTEM_CODE": "01000", "RF_UNIT": "D", "RF_STATE": "OFF" },

    # System code 01001
    4523345: { "RF_SYSTEM_CODE": "01001", "RF_UNIT": "A", "RF_STATE": "ON" },
    4523348: { "RF_SYSTEM_CODE": "01001", "RF_UNIT": "A", "RF_STATE": "OFF" },
    4526417: { "RF_SYSTEM_CODE": "01001", "RF_UNIT": "B", "RF_STATE": "ON" },
    4526420: { "RF_SYSTEM_CODE": "01001", "RF_UNIT": "B", "RF_STATE": "OFF" },
    4527185: { "RF_SYSTEM_CODE": "01001", "RF_UNIT": "C", "RF_STATE": "ON" },
    4527188: { "RF_SYSTEM_CODE": "01001", "RF_UNIT": "C", "RF_STATE": "OFF" },
    4527377: { "RF_SYSTEM_CODE": "01001", "RF_UNIT": "D", "RF_STATE": "ON" },
    4527380: { "RF_SYSTEM_CODE": "01001", "RF_UNIT": "D", "RF_STATE": "OFF" },

    # System code 01010
    4474193: { "RF_SYSTEM_CODE": "01010", "RF_UNIT": "A", "RF_STATE": "ON" },
    4474196: { "RF_SYSTEM_CODE": "01010", "RF_UNIT": "A", "RF_STATE": "OFF" },
    4477265: { "RF_SYSTEM_CODE": "01010", "RF_UNIT": "B", "RF_STATE": "ON" },
    4477268: { "RF_SYSTEM_CODE": "01010", "RF_UNIT": "B", "RF_STATE": "OFF" },
    4478033: { "RF_SYSTEM_CODE": "01010", "RF_UNIT": "C", "RF_STATE": "ON" },
    4478036: { "RF_SYSTEM_CODE": "01010", "RF_UNIT": "C", "RF_STATE": "OFF" },
    4478225: { "RF_SYSTEM_CODE": "01010", "RF_UNIT": "D", "RF_STATE": "ON" },
    4478228: { "RF_SYSTEM_CODE": "01010", "RF_UNIT": "D", "RF_STATE": "OFF" },

    # System code 01011
    4457809: { "RF_SYSTEM_CODE": "01011", "RF_UNIT": "A", "RF_STATE": "ON" },
    4457812: { "RF_SYSTEM_CODE": "01011", "RF_UNIT": "A", "RF_STATE": "OFF" },
    4460881: { "RF_SYSTEM_CODE": "01011", "RF_UNIT": "B", "RF_STATE": "ON" },
    4460884: { "RF_SYSTEM_CODE": "01011", "RF_UNIT": "B", "RF_STATE": "OFF" },
    4461649: { "RF_SYSTEM_CODE": "01011", "RF_UNIT": "C", "RF_STATE": "ON" },
    4461652: { "RF_SYSTEM_CODE": "01011", "RF_UNIT": "C", "RF_STATE": "OFF" },
    4461841: { "RF_SYSTEM_CODE": "01011", "RF_UNIT": "D", "RF_STATE": "ON" },
    4461844: { "RF_SYSTEM_CODE": "01011", "RF_UNIT": "D", "RF_STATE": "OFF" },

    # System code 01100
    4277585: { "RF_SYSTEM_CODE": "01100", "RF_UNIT": "A", "RF_STATE": "ON" },
    4277588: { "RF_SYSTEM_CODE": "01100", "RF_UNIT": "A", "RF_STATE": "OFF" },
    4280657: { "RF_SYSTEM_CODE": "01100", "RF_UNIT": "B", "RF_STATE": "ON" },
    4280660: { "RF_SYSTEM_CODE": "01100", "RF_UNIT": "B", "RF_STATE": "OFF" },
    4281425: { "RF_SYSTEM_CODE": "01100", "RF_UNIT": "C", "RF_STATE": "ON" },
    4281428: { "RF_SYSTEM_CODE": "01100", "RF_UNIT": "C", "RF_STATE": "OFF" },
    4281617: { "RF_SYSTEM_CODE": "01100", "RF_UNIT": "D", "RF_STATE": "ON" },
    4281620: { "RF_SYSTEM_CODE": "01100", "RF_UNIT": "D", "RF_STATE": "OFF" },

    # System code 01101
    4261201: { "RF_SYSTEM_CODE": "01101", "RF_UNIT": "A", "RF_STATE": "ON" },
    4261204: { "RF_SYSTEM_CODE": "01101", "RF_UNIT": "A", "RF_STATE": "OFF" },
    4264273: { "RF_SYSTEM_CODE": "01101", "RF_UNIT": "B", "RF_STATE": "ON" },
    4264276: { "RF_SYSTEM_CODE": "01101", "RF_UNIT": "B", "RF_STATE": "OFF" },
    4265041: { "RF_SYSTEM_CODE": "01101", "RF_UNIT": "C", "RF_STATE": "ON" },
    4265044: { "RF_SYSTEM_CODE": "01101", "RF_UNIT": "C", "RF_STATE": "OFF" },
    4265233: { "RF_SYSTEM_CODE": "01101", "RF_UNIT": "D", "RF_STATE": "ON" },
    4265236: { "RF_SYSTEM_CODE": "01101", "RF_UNIT": "D", "RF_STATE": "OFF" },

    # System code 01110
    4212049: { "RF_SYSTEM_CODE": "01110", "RF_UNIT": "A", "RF_STATE": "ON" },
    4212052: { "RF_SYSTEM_CODE": "01110", "RF_UNIT": "A", "RF_STATE": "OFF" },
    4215121: { "RF_SYSTEM_CODE": "01110", "RF_UNIT": "B", "RF_STATE": "ON" },
    4215124: { "RF_SYSTEM_CODE": "01110", "RF_UNIT": "B", "RF_STATE": "OFF" },
    4215889: { "RF_SYSTEM_CODE": "01110", "RF_UNIT": "C", "RF_STATE": "ON" },
    4215892: { "RF_SYSTEM_CODE": "01110", "RF_UNIT": "C", "RF_STATE": "OFF" },
    4216081: { "RF_SYSTEM_CODE": "01110", "RF_UNIT": "D", "RF_STATE": "ON" },
    4216084: { "RF_SYSTEM_CODE": "01110", "RF_UNIT": "D", "RF_STATE": "OFF" },

    # System code 01111
    4195665: { "RF_SYSTEM_CODE": "01111", "RF_UNIT": "A", "RF_STATE": "ON" },
    4195668: { "RF_SYSTEM_CODE": "01111", "RF_UNIT": "A", "RF_STATE": "OFF" },
    4198737: { "RF_SYSTEM_CODE": "01111", "RF_UNIT": "B", "RF_STATE": "ON" },
    4198740: { "RF_SYSTEM_CODE": "01111", "RF_UNIT": "B", "RF_STATE": "OFF" },
    4199505: { "RF_SYSTEM_CODE": "01111", "RF_UNIT": "C", "RF_STATE": "ON" },
    4199508: { "RF_SYSTEM_CODE": "01111", "RF_UNIT": "C", "RF_STATE": "OFF" },
    4199697: { "RF_SYSTEM_CODE": "01111", "RF_UNIT": "D", "RF_STATE": "ON" },
    4199700: { "RF_SYSTEM_CODE": "01111", "RF_UNIT": "D", "RF_STATE": "OFF" },

    # System code 10000
    1394001: { "RF_SYSTEM_CODE": "10000", "RF_UNIT": "A", "RF_STATE": "ON" },
    1394004: { "RF_SYSTEM_CODE": "10000", "RF_UNIT": "A", "RF_STATE": "OFF" },
    1397073: { "RF_SYSTEM_CODE": "10000", "RF_UNIT": "B", "RF_STATE": "ON" },
    1397076: { "RF_SYSTEM_CODE": "10000", "RF_UNIT": "B", "RF_STATE": "OFF" },
    1397841: { "RF_SYSTEM_CODE": "10000", "RF_UNIT": "C", "RF_STATE": "ON" },
    1397844: { "RF_SYSTEM_CODE": "10000", "RF_UNIT": "C", "RF_STATE": "OFF" },
    1398033: { "RF_SYSTEM_CODE": "10000", "RF_UNIT": "D", "RF_STATE": "ON" },
    1398036: { "RF_SYSTEM_CODE": "10000", "RF_UNIT": "D", "RF_STATE": "OFF" },

    # System code 10001
    1377617: { "RF_SYSTEM_CODE": "10001", "RF_UNIT": "A", "RF_STATE": "ON" },
    1377620: { "RF_SYSTEM_CODE": "10001", "RF_UNIT": "A", "RF_STATE": "OFF" },
    1380689: { "RF_SYSTEM_CODE": "10001", "RF_UNIT": "B", "RF_STATE": "ON" },
    1380692: { "RF_SYSTEM_CODE": "10001", "RF_UNIT": "B", "RF_STATE": "OFF" },
    1381457: { "RF_SYSTEM_CODE": "10001", "RF_UNIT": "C", "RF_STATE": "ON" },
    1381460: { "RF_SYSTEM_CODE": "10001", "RF_UNIT": "C", "RF_STATE": "OFF" },
    1381649: { "RF_SYSTEM_CODE": "10001", "RF_UNIT": "D", "RF_STATE": "ON" },
    1381652: { "RF_SYSTEM_CODE": "10001", "RF_UNIT": "D", "RF_STATE": "OFF" },

    # System code 10010
    1328465: { "RF_SYSTEM_CODE": "10010", "RF_UNIT": "A", "RF_STATE": "ON" },
    1328468: { "RF_SYSTEM_CODE": "10010", "RF_UNIT": "A", "RF_STATE": "OFF" },
    1331537: { "RF_SYSTEM_CODE": "10010", "RF_UNIT": "B", "RF_STATE": "ON" },
    1331540: { "RF_SYSTEM_CODE": "10010", "RF_UNIT": "B", "RF_STATE": "OFF" },
    1332305: { "RF_SYSTEM_CODE": "10010", "RF_UNIT": "C", "RF_STATE": "ON" },
    1332308: { "RF_SYSTEM_CODE": "10010", "RF_UNIT": "C", "RF_STATE": "OFF" },
    1332497: { "RF_SYSTEM_CODE": "10010", "RF_UNIT": "D", "RF_STATE": "ON" },
    1332500: { "RF_SYSTEM_CODE": "10010", "RF_UNIT": "D", "RF_STATE": "OFF" },

    # System code 10011
    1312081: { "RF_SYSTEM_CODE": "10011", "RF_UNIT": "A", "RF_STATE": "ON" },
    1312084: { "RF_SYSTEM_CODE": "10011", "RF_UNIT": "A", "RF_STATE": "OFF" },
    1315153: { "RF_SYSTEM_CODE": "10011", "RF_UNIT": "B", "RF_STATE": "ON" },
    1315156: { "RF_SYSTEM_CODE": "10011", "RF_UNIT": "B", "RF_STATE": "OFF" },
    1315921: { "RF_SYSTEM_CODE": "10011", "RF_UNIT": "C", "RF_STATE": "ON" },
    1315924: { "RF_SYSTEM_CODE": "10011", "RF_UNIT": "C", "RF_STATE": "OFF" },
    1316113: { "RF_SYSTEM_CODE": "10011", "RF_UNIT": "D", "RF_STATE": "ON" },
    1316116: { "RF_SYSTEM_CODE": "10011", "RF_UNIT": "D", "RF_STATE": "OFF" },

    # System code 10100
    1131857: { "RF_SYSTEM_CODE": "10100", "RF_UNIT": "A", "RF_STATE": "ON" },
    1131860: { "RF_SYSTEM_CODE": "10100", "RF_UNIT": "A", "RF_STATE": "OFF" },
    1134929: { "RF_SYSTEM_CODE": "10100", "RF_UNIT": "B", "RF_STATE": "ON" },
    1134932: { "RF_SYSTEM_CODE": "10100", "RF_UNIT": "B", "RF_STATE": "OFF" },
    1135697: { "RF_SYSTEM_CODE": "10100", "RF_UNIT": "C", "RF_STATE": "ON" },
    1135700: { "RF_SYSTEM_CODE": "10100", "RF_UNIT": "C", "RF_STATE": "OFF" },
    1135889: { "RF_SYSTEM_CODE": "10100", "RF_UNIT": "D", "RF_STATE": "ON" },
    1135892: { "RF_SYSTEM_CODE": "10100", "RF_UNIT": "D", "RF_STATE": "OFF" },

    # System code 10101
    1115473: { "RF_SYSTEM_CODE": "10101", "RF_UNIT": "A", "RF_STATE": "ON" },
    1115476: { "RF_SYSTEM_CODE": "10101", "RF_UNIT": "A", "RF_STATE": "OFF" },
    1118545: { "RF_SYSTEM_CODE": "10101", "RF_UNIT": "B", "RF_STATE": "ON" },
    1118548: { "RF_SYSTEM_CODE": "10101", "RF_UNIT": "B", "RF_STATE": "OFF" },
    1119313: { "RF_SYSTEM_CODE": "10101", "RF_UNIT": "C", "RF_STATE": "ON" },
    1119316: { "RF_SYSTEM_CODE": "10101", "RF_UNIT": "C", "RF_STATE": "OFF" },
    1119505: { "RF_SYSTEM_CODE": "10101", "RF_UNIT": "D", "RF_STATE": "ON" },
    1119508: { "RF_SYSTEM_CODE": "10101", "RF_UNIT": "D", "RF_STATE": "OFF" },

    # System code 10110
    1066321: { "RF_SYSTEM_CODE": "10110", "RF_UNIT": "A", "RF_STATE": "ON" },
    1066324: { "RF_SYSTEM_CODE": "10110", "RF_UNIT": "A", "RF_STATE": "OFF" },
    1069393: { "RF_SYSTEM_CODE": "10110", "RF_UNIT": "B", "RF_STATE": "ON" },
    1069396: { "RF_SYSTEM_CODE": "10110", "RF_UNIT": "B", "RF_STATE": "OFF" },
    1070161: { "RF_SYSTEM_CODE": "10110", "RF_UNIT": "C", "RF_STATE": "ON" },
    1070164: { "RF_SYSTEM_CODE": "10110", "RF_UNIT": "C", "RF_STATE": "OFF" },
    1070353: { "RF_SYSTEM_CODE": "10110", "RF_UNIT": "D", "RF_STATE": "ON" },
    1070356: { "RF_SYSTEM_CODE": "10110", "RF_UNIT": "D", "RF_STATE": "OFF" },

    # System code 10111
    1049937: { "RF_SYSTEM_CODE": "10111", "RF_UNIT": "A", "RF_STATE": "ON" },
    1049940: { "RF_SYSTEM_CODE": "10111", "RF_UNIT": "A", "RF_STATE": "OFF" },
    1053009: { "RF_SYSTEM_CODE": "10111", "RF_UNIT": "B", "RF_STATE": "ON" },
    1053012: { "RF_SYSTEM_CODE": "10111", "RF_UNIT": "B", "RF_STATE": "OFF" },
    1053777: { "RF_SYSTEM_CODE": "10111", "RF_UNIT": "C", "RF_STATE": "ON" },
    1053780: { "RF_SYSTEM_CODE": "10111", "RF_UNIT": "C", "RF_STATE": "OFF" },
    1053969: { "RF_SYSTEM_CODE": "10111", "RF_UNIT": "D", "RF_STATE": "ON" },
    1053972: { "RF_SYSTEM_CODE": "10111", "RF_UNIT": "D", "RF_STATE": "OFF" },

    # System code 11000
    345425: { "RF_SYSTEM_CODE": "11000", "RF_UNIT": "A", "RF_STATE": "ON" },
    345428: { "RF_SYSTEM_CODE": "11000", "RF_UNIT": "A", "RF_STATE": "OFF" },
    348497: { "RF_SYSTEM_CODE": "11000", "RF_UNIT": "B", "RF_STATE": "ON" },
    348500: { "RF_SYSTEM_CODE": "11000", "RF_UNIT": "B", "RF_STATE": "OFF" },
    349265: { "RF_SYSTEM_CODE": "11000", "RF_UNIT": "C", "RF_STATE": "ON" },
    349268: { "RF_SYSTEM_CODE": "11000", "RF_UNIT": "C", "RF_STATE": "OFF" },
    349457: { "RF_SYSTEM_CODE": "11000", "RF_UNIT": "D", "RF_STATE": "ON" },
    349460: { "RF_SYSTEM_CODE": "11000", "RF_UNIT": "D", "RF_STATE": "OFF" },

    # System code 11001
    329041: { "RF_SYSTEM_CODE": "11001", "RF_UNIT": "A", "RF_STATE": "ON" },
    329044: { "RF_SYSTEM_CODE": "11001", "RF_UNIT": "A", "RF_STATE": "OFF" },
    332113: { "RF_SYSTEM_CODE": "11001", "RF_UNIT": "B", "RF_STATE": "ON" },
    332116: { "RF_SYSTEM_CODE": "11001", "RF_UNIT": "B", "RF_STATE": "OFF" },
    332881: { "RF_SYSTEM_CODE": "11001", "RF_UNIT": "C", "RF_STATE": "ON" },
    332884: { "RF_SYSTEM_CODE": "11001", "RF_UNIT": "C", "RF_STATE": "OFF" },
    333073: { "RF_SYSTEM_CODE": "11001", "RF_UNIT": "D", "RF_STATE": "ON" },
    333076: { "RF_SYSTEM_CODE": "11001", "RF_UNIT": "D", "RF_STATE": "OFF" },

    # System code 11010
    279889: { "RF_SYSTEM_CODE": "11010", "RF_UNIT": "A", "RF_STATE": "ON" },
    279892: { "RF_SYSTEM_CODE": "11010", "RF_UNIT": "A", "RF_STATE": "OFF" },
    282961: { "RF_SYSTEM_CODE": "11010", "RF_UNIT": "B", "RF_STATE": "ON" },
    282964: { "RF_SYSTEM_CODE": "11010", "RF_UNIT": "B", "RF_STATE": "OFF" },
    283729: { "RF_SYSTEM_CODE": "11010", "RF_UNIT": "C", "RF_STATE": "ON" },
    283732: { "RF_SYSTEM_CODE": "11010", "RF_UNIT": "C", "RF_STATE": "OFF" },
    283921: { "RF_SYSTEM_CODE": "11010", "RF_UNIT": "D", "RF_STATE": "ON" },
    283924: { "RF_SYSTEM_CODE": "11010", "RF_UNIT": "D", "RF_STATE": "OFF" },

    # System code 11011
    263505: { "RF_SYSTEM_CODE": "11011", "RF_UNIT": "A", "RF_STATE": "ON" },
    263508: { "RF_SYSTEM_CODE": "11011", "RF_UNIT": "A", "RF_STATE": "OFF" },
    266577: { "RF_SYSTEM_CODE": "11011", "RF_UNIT": "B", "RF_STATE": "ON" },
    266580: { "RF_SYSTEM_CODE": "11011", "RF_UNIT": "B", "RF_STATE": "OFF" },
    267345: { "RF_SYSTEM_CODE": "11011", "RF_UNIT": "C", "RF_STATE": "ON" },
    267348: { "RF_SYSTEM_CODE": "11011", "RF_UNIT": "C", "RF_STATE": "OFF" },
    267537: { "RF_SYSTEM_CODE": "11011", "RF_UNIT": "D", "RF_STATE": "ON" },
    267540: { "RF_SYSTEM_CODE": "11011", "RF_UNIT": "D", "RF_STATE": "OFF" },

    # System code 11100
    83281: { "RF_SYSTEM_CODE": "11100", "RF_UNIT": "A", "RF_STATE": "ON" },
    83284: { "RF_SYSTEM_CODE": "11100", "RF_UNIT": "A", "RF_STATE": "OFF" },
    86353: { "RF_SYSTEM_CODE": "11100", "RF_UNIT": "B", "RF_STATE": "ON" },
    86356: { "RF_SYSTEM_CODE": "11100", "RF_UNIT": "B", "RF_STATE": "OFF" },
    87121: { "RF_SYSTEM_CODE": "11100", "RF_UNIT": "C", "RF_STATE": "ON" },
    87124: { "RF_SYSTEM_CODE": "11100", "RF_UNIT": "C", "RF_STATE": "OFF" },
    87313: { "RF_SYSTEM_CODE": "11100", "RF_UNIT": "D", "RF_STATE": "ON" },
    87316: { "RF_SYSTEM_CODE": "11100", "RF_UNIT": "D", "RF_STATE": "OFF" },

    # System code 11101
    66897: { "RF_SYSTEM_CODE": "11101", "RF_UNIT": "A", "RF_STATE": "ON" },
    66900: { "RF_SYSTEM_CODE": "11101", "RF_UNIT": "A", "RF_STATE": "OFF" },
    69969: { "RF_SYSTEM_CODE": "11101", "RF_UNIT": "B", "RF_STATE": "ON" },
    69972: { "RF_SYSTEM_CODE": "11101", "RF_UNIT": "B", "RF_STATE": "OFF" },
    70737: { "RF_SYSTEM_CODE": "11101", "RF_UNIT": "C", "RF_STATE": "ON" },
    70740: { "RF_SYSTEM_CODE": "11101", "RF_UNIT": "C", "RF_STATE": "OFF" },
    70929: { "RF_SYSTEM_CODE": "11101", "RF_UNIT": "D", "RF_STATE": "ON" },
    70932: { "RF_SYSTEM_CODE": "11101", "RF_UNIT": "D", "RF_STATE": "OFF" },

    # System code 11110
    17745: { "RF_SYSTEM_CODE": "11110", "RF_UNIT": "A", "RF_STATE": "ON" },
    17748: { "RF_SYSTEM_CODE": "11110", "RF_UNIT": "A", "RF_STATE": "OFF" },
    20817: { "RF_SYSTEM_CODE": "11110", "RF_UNIT": "B", "RF_STATE": "ON" },
    20820: { "RF_SYSTEM_CODE": "11110", "RF_UNIT": "B", "RF_STATE": "OFF" },
    21585: { "RF_SYSTEM_CODE": "11110", "RF_UNIT": "C", "RF_STATE": "ON" },
    21588: { "RF_SYSTEM_CODE": "11110", "RF_UNIT": "C", "RF_STATE": "OFF" },
    21777: { "RF_SYSTEM_CODE": "11110", "RF_UNIT": "D", "RF_STATE": "ON" },
    21780: { "RF_SYSTEM_CODE": "11110", "RF_UNIT": "D", "RF_STATE": "OFF" },

    # System code 11111
    1361: { "RF_SYSTEM_CODE": "11111", "RF_UNIT": "A", "RF_STATE": "ON" },
    1364: { "RF_SYSTEM_CODE": "11111", "RF_UNIT": "A", "RF_STATE": "OFF" },
    4433: { "RF_SYSTEM_CODE": "11111", "RF_UNIT": "B", "RF_STATE": "ON" },
    4436: { "RF_SYSTEM_CODE": "11111", "RF_UNIT": "B", "RF_STATE": "OFF" },
    5201: { "RF_SYSTEM_CODE": "11111", "RF_UNIT": "C", "RF_STATE": "ON" },
    5204: { "RF_SYSTEM_CODE": "11111", "RF_UNIT": "C", "RF_STATE": "OFF" },
    5393: { "RF_SYSTEM_CODE": "11111", "RF_UNIT": "D", "RF_STATE": "ON" },
    5396: { "RF_SYSTEM_CODE": "11111", "RF_UNIT": "D", "RF_STATE": "OFF" },

}

# TX - from specified system code and unit state to RF code
TX = {

    # System code 00000
    "00000": {
        "A": { "ON": 5588305, "OFF": 5588308 },
        "B": { "ON": 5591377, "OFF": 5591380 },
        "C": { "ON": 5592145, "OFF": 5592148 },
        "D": { "ON": 5592337, "OFF": 5592340 },
    },

    # System code 00001
    "00001": {
        "A": { "ON": 5571921, "OFF": 5571924 },
        "B": { "ON": 5574993, "OFF": 5574996 },
        "C": { "ON": 5575761, "OFF": 5575764 },
        "D": { "ON": 5575953, "OFF": 5575956 },
    },

    # System code 00010
    "00010": {
        "A": { "ON": 5522769, "OFF": 5522772 },
        "B": { "ON": 5525841, "OFF": 5525844 },
        "C": { "ON": 5526609, "OFF": 5526612 },
        "D": { "ON": 5526801, "OFF": 5526804 },
    },

    # System code 00011
    "00011": {
        "A": { "ON": 5506385, "OFF": 5506388 },
        "B": { "ON": 5509457, "OFF": 5509460 },
        "C": { "ON": 5510225, "OFF": 5510228 },
        "D": { "ON": 5510417, "OFF": 5510420 },
    },

    # System code 00100
    "00100": {
        "A": { "ON": 5326161, "OFF": 5326164 },
        "B": { "ON": 5329233, "OFF": 5329236 },
        "C": { "ON": 5330001, "OFF": 5330004 },
        "D": { "ON": 5330193, "OFF": 5330196 },
    },

    # System code 00101
    "00101": {
        "A": { "ON": 5309777, "OFF": 5309780 },
        "B": { "ON": 5312849, "OFF": 5312852 },
        "C": { "ON": 5313617, "OFF": 5313620 },
        "D": { "ON": 5313809, "OFF": 5313812 },
    },

    # System code 00110
    "00110": {
        "A": { "ON": 5260625, "OFF": 5260628 },
        "B": { "ON": 5263697, "OFF": 5263700 },
        "C": { "ON": 5264465, "OFF": 5264468 },
        "D": { "ON": 5264657, "OFF": 5264660 },
    },

    # System code 00111
    "00111": {
        "A": { "ON": 5244241, "OFF": 5244244 },
        "B": { "ON": 5247313, "OFF": 5247316 },
        "C": { "ON": 5248081, "OFF": 5248084 },
        "D": { "ON": 5248273, "OFF": 5248276 },
    },

    # System code 01000
    "01000": {
        "A": { "ON": 4539729, "OFF": 4539732 },
        "B": { "ON": 4542801, "OFF": 4542804 },
        "C": { "ON": 4543569, "OFF": 4543572 },
        "D": { "ON": 4543761, "OFF": 4543764 },
    },

    # System code 01001
    "01001": {
        "A": { "ON": 4523345, "OFF": 4523348 },
        "B": { "ON": 4526417, "OFF": 4526420 },
        "C": { "ON": 4527185, "OFF": 4527188 },
        "D": { "ON": 4527377, "OFF": 4527380 },
    },

    # System code 01010
    "01010": {
        "A": { "ON": 4474193, "OFF": 4474196 },
        "B": { "ON": 4477265, "OFF": 4477268 },
        "C": { "ON": 4478033, "OFF": 4478036 },
        "D": { "ON": 4478225, "OFF": 4478228 },
    },

    # System code 01011
    "01011": {
        "A": { "ON": 4457809, "OFF": 4457812 },
        "B": { "ON": 4460881, "OFF": 4460884 },
        "C": { "ON": 4461649, "OFF": 4461652 },
        "D": { "ON": 4461841, "OFF": 4461844 },
    },

    # System code 01100
    "01100": {
        "A": { "ON": 4277585, "OFF": 4277588 },
        "B": { "ON": 4280657, "OFF": 4280660 },
        "C": { "ON": 4281425, "OFF": 4281428 },
        "D": { "ON": 4281617, "OFF": 4281620 },
    },

    # System code 01101
    "01101": {
        "A": { "ON": 4261201, "OFF": 4261204 },
        "B": { "ON": 4264273, "OFF": 4264276 },
        "C": { "ON": 4265041, "OFF": 4265044 },
        "D": { "ON": 4265233, "OFF": 4265236 },
    },

    # System code 01110
    "01110": {
        "A": { "ON": 4212049, "OFF": 4212052 },
        "B": { "ON": 4215121, "OFF": 4215124 },
        "C": { "ON": 4215889, "OFF": 4215892 },
        "D": { "ON": 4216081, "OFF": 4216084 },
    },

    # System code 01111
    "01111": {
        "A": { "ON": 4195665, "OFF": 4195668 },
        "B": { "ON": 4198737, "OFF": 4198740 },
        "C": { "ON": 4199505, "OFF": 4199508 },
        "D": { "ON": 4199697, "OFF": 4199700 },
    },

    # System code 10000
    "10000": {
        "A": { "ON": 1394001, "OFF": 1394004 },
        "B": { "ON": 1397073, "OFF": 1397076 },
        "C": { "ON": 1397841, "OFF": 1397844 },
        "D": { "ON": 1398033, "OFF": 1398036 },
    },

    # System code 10001
    "10001": {
        "A": { "ON": 1377617, "OFF": 1377620 },
        "B": { "ON": 1380689, "OFF": 1380692 },
        "C": { "ON": 1381457, "OFF": 1381460 },
        "D": { "ON": 1381649, "OFF": 1381652 },
    },

    # System code 10010
    "10010": {
        "A": { "ON": 1328465, "OFF": 1328468 },
        "B": { "ON": 1331537, "OFF": 1331540 },
        "C": { "ON": 1332305, "OFF": 1332308 },
        "D": { "ON": 1332497, "OFF": 1332500 },
    },

    # System code 10011
    "10011": {
        "A": { "ON": 1312081, "OFF": 1312084 },
        "B": { "ON": 1315153, "OFF": 1315156 },
        "C": { "ON": 1315921, "OFF": 1315924 },
        "D": { "ON": 1316113, "OFF": 1316116 },
    },

    # System code 10100
    "10100": {
        "A": { "ON": 1131857, "OFF": 1131860 },
        "B": { "ON": 1134929, "OFF": 1134932 },
        "C": { "ON": 1135697, "OFF": 1135700 },
        "D": { "ON": 1135889, "OFF": 1135892 },
    },

    # System code 10101
    "10101": {
        "A": { "ON": 1115473, "OFF": 1115476 },
        "B": { "ON": 1118545, "OFF": 1118548 },
        "C": { "ON": 1119313, "OFF": 1119316 },
        "D": { "ON": 1119505, "OFF": 1119508 },
    },

    # System code 10110
    "10110": {
        "A": { "ON": 1066321, "OFF": 1066324 },
        "B": { "ON": 1069393, "OFF": 1069396 },
        "C": { "ON": 1070161, "OFF": 1070164 },
        "D": { "ON": 1070353, "OFF": 1070356 },
    },

    # System code 10111
    "10111": {
        "A": { "ON": 1049937, "OFF": 1049940 },
        "B": { "ON": 1053009, "OFF": 1053012 },
        "C": { "ON": 1053777, "OFF": 1053780 },
        "D": { "ON": 1053969, "OFF": 1053972 },
    },

    # System code 11000
    "11000": {
        "A": { "ON": 345425, "OFF": 345428 },
        "B": { "ON": 348497, "OFF": 348500 },
        "C": { "ON": 349265, "OFF": 349268 },
        "D": { "ON": 349457, "OFF": 349460 },
    },

    # System code 11001
    "11001": {
        "A": { "ON": 329041, "OFF": 329044 },
        "B": { "ON": 332113, "OFF": 332116 },
        "C": { "ON": 332881, "OFF": 332884 },
        "D": { "ON": 333073, "OFF": 333076 },
    },

    # System code 11010
    "11010": {
        "A": { "ON": 279889, "OFF": 279892 },
        "B": { "ON": 282961, "OFF": 282964 },
        "C": { "ON": 283729, "OFF": 283732 },
        "D": { "ON": 283921, "OFF": 283924 },
    },

    # System code 11011
    "11011": {
        "A": { "ON": 263505, "OFF": 263508 },
        "B": { "ON": 266577, "OFF": 266580 },
        "C": { "ON": 267345, "OFF": 267348 },
        "D": { "ON": 267537, "OFF": 267540 },
    },

    # System code 11100
    "11100": {
        "A": { "ON": 83281, "OFF": 83284 },
        "B": { "ON": 86353, "OFF": 86356 },
        "C": { "ON": 87121, "OFF": 87124 },
        "D": { "ON": 87313, "OFF": 87316 },
    },

    # System code 11101
    "11101": {
        "A": { "ON": 66897, "OFF": 66900 },
        "B": { "ON": 69969, "OFF": 69972 },
        "C": { "ON": 70737, "OFF": 70740 },
        "D": { "ON": 70929, "OFF": 70932 },
    },

    # System code 11110
    "11110": {
        "A": { "ON": 17745, "OFF": 17748 },
        "B": { "ON": 20817, "OFF": 20820 },
        "C": { "ON": 21585, "OFF": 21588 },
        "D": { "ON": 21777, "OFF": 21780 },
    },

    # System code 11111
    "11111": {
        "A": { "ON": 1361, "OFF": 1364 },
        "B": { "ON": 4433, "OFF": 4436 },
        "C": { "ON": 5201, "OFF": 5204 },
        "D": { "ON": 5393, "OFF": 5396 },
    },

}
